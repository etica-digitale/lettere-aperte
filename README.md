Lettere aperte del gruppo di divulgazione volontario Etica Digitale

<a href="https://liberapay.com/EticaDigitale/"><img src="https://i.imgur.com/4B2PxjP.png"/></a>  

### Contatti

[Sito](https://eticadigitale.org/)  
[Canale Telegram](https://t.me/eticadigitalechannel)  
[Gruppo Telegram](https://t.me/EticaDigitale)  
mail: eticadigitale@protonmail.com
