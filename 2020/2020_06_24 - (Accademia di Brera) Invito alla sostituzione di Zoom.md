> *il seguente documento è una mail inviata in data 24/06/2020 da Etica Digitale (eticadigitale@protonmail.com) alla segreteria dell’Accademia di Brera (segreteria.direzione@accademiadibrera.milano.it), dal titolo* “Invito alla sostituzione di Zoom“.
Il documento è disponibile per il download sia in formato [.odt](https://gitlab.com/etica-digitale/lettere-aperte/-/raw/master/2020/2020_06_24%20-%20(Accademia%20di%20Brera)%20Invito%20alla%20sostituzione%20di%20Zoom.odt) che [.pdf](https://gitlab.com/etica-digitale/lettere-aperte/-/raw/master/2020/2020_06_24%20-%20(Accademia%20di%20Brera)%20Invito%20alla%20sostituzione%20di%20Zoom.pdf?inline=false).

Stimata Accademia di Brera,  
siamo Etica Digitale, un gruppo di volontari che si occupano della divulgazione di temi inerenti appunto all'etica digitale.  
  
Ci è giunta voce che per il test obbligatorio di Fondamenti di Informatica, ambito Tecnologie per l'informatica (o, nel sito dell'accademia, Corso di Autoformazione Web Based - Syllabus ECDL 5) in data 11 luglio verrà utilizzato il software Zoom come riportato negli screenshot segnalatici da alcuni Vostri studenti (zip allegato [*qui rimosso*]). Ci duole informarvi che negli ultimi mesi Zoom è stato dimostrato essere in mano a un'azienda che non ha costruito la piattaforma mettendo la privacy al primo posto, come dimostrano i seguenti articoli:  
1. Dichiarazioni fuorvianti sul proprio sistema di crittografia, con rischio causa:  
https://theintercept.com/2020/03/31/zoom-meeting-encryption/  
2. Uno dei peggiori sistemi di crittografia in circolazione, con allacciamenti a server che nulla c'entravano con alcune chiamate:  
https://theintercept.com/2020/04/03/zooms-encryption-is-not-suited-for-secrets-and-has-surprising-links-to-china-researchers-discover/  
3. Possibilità di accedere a mail di sconosciuti grazie a falle nel codice:  
https://www.repubblica.it/tecnologia/sicurezza/2020/04/01/news/zoom_dalla_privacy_ai_troll_tutti_i_guai_dell_app_per_videochat-252877928/

Come se non bastasse, hanno reso la privacy un diritto per pochi e hanno censurato attivisti per aver infranto leggi inesistenti:  

4. Annuncio dell'implementazione di crittografia end-to-end (quella che non permette loro di sapere cosa succede), ma solo per gli utenti paganti:  
https://www.theguardian.com/technology/2020/jun/03/zoom-privacy-law-enforcement-technology-yuan  
5. Sospensione account di attivisti cinesi per una veglia in ricordo di Piazza Tiananmen 1989 per rispettare la legge cinese (che non vieta simili veglie)  
https://gizmodo.com/zoom-shuts-down-activist-group-s-account-after-its-tian-1843993694

Nonostante l'azienda sia corsa ai ripari per attività come lo "zoombombing" (spiegato nella fonte n°3) e abbia ripristinato gli account degli attivisti, la loro privacy policy rimane incentrata sulla raccolta di dati tramite cookie, fingerprinting e strumenti di analytics a scopi commerciali; come dichiarato dall'azienda stessa:  
https://zoom.us/privacy/  
  
Speriamo conveniate con noi che la didattica non dovrebbe diventare una mercificazione dello studente (come neanche dell'insegnante) per mano di una compagnia che raccoglie informazioni a fini commerciali e fa della privacy un lusso. Lo studente deve imparare nel pieno rispetto dei suoi diritti, senza dover essere costretto alla scelta tra il diritto alla privacy ed il diritto all'apprendimento, entrambi garantiti dalla legge della Repubblica Italiana. Vi chiediamo dunque se voleste prendere in considerazione un abbandono della piattaforma Zoom, consultandoVi con i Vostri tecnici per una scelta più opportuna, e se poteste inoltre fornirci delucidazioni su come verranno gestiti i dati sensibili dei documenti richiesti agli studenti (CI e CF) da mostrare tramite Zoom, in concomitanza con la normativa europea per il trattamento dei dati personali.  
  
Alcune alternative etiche:  
https://eticadigitale.org/2020/03/13/alternative-digitali-ai-tempi-del-covid-19/  
Lettera del Professore Angelo Raffaele Meo alla Ministra dell'Istruzione Azzolina in appoggio al software libero:  
https://scuolalibera.continuity.space/lettera-professor-meo  
  
  
Cordiali saluti,  
Etica Digitale

